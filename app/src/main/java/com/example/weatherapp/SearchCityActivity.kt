package com.example.weatherapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import androidx.core.content.ContextCompat.startActivity
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONObject


class SearchCityActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val mCities = remember { mutableStateOf(listOf("")) }
            val searchRequest = remember { mutableStateOf("") }

            MyContent(mCities, searchRequest, this, savedInstanceState)
        }
    }
}
@Composable
fun MyContent(
    mCities: MutableState<List<String>>,
    searchRequest: MutableState<String>,
    context: Context,
    savedInstanceState: Bundle?
){
    var mExpanded by remember { mutableStateOf(false) }

    var mTextFieldSize by remember { mutableStateOf(Size.Zero) }

    val icon = if (mExpanded)
        Icons.Filled.KeyboardArrowUp
    else
        Icons.Filled.KeyboardArrowDown

    val mSelectedText: MutableState<String> = searchRequest

    Column(Modifier.padding(20.dp)) {
        OutlinedTextField(
            value = mSelectedText.value,
            onValueChange = { mSelectedText.value = it },
            modifier = Modifier
                .fillMaxWidth()
                .onGloballyPositioned { coordinates ->
                    mTextFieldSize = coordinates.size.toSize()
                },
            label = {Text("Start enter the city here")},
            trailingIcon = {
                Icon(icon,"contentDescription",
                    Modifier.clickable { mExpanded = !mExpanded })
            }
        )
        getData(searchRequest, context, mCities)

        Box() {
            DropdownMenu(
                expanded = mExpanded,
                onDismissRequest = { mExpanded = false },
                modifier = Modifier.width(with(LocalDensity.current){mTextFieldSize.width.toDp()})
            ) {
                mCities.value.forEach { label -> DropdownMenuItem(onClick = {
                    mSelectedText.value = label
                    mExpanded = false
                    Log.d("Work", "mSelectedText = ${mSelectedText.value}")
                }) {
                    Text(text = label)
                }
                }
            }
        }
        Column() {
            Button(modifier = Modifier.width(100.dp).padding(start = 7.dp, top = 25.dp, end = 7.dp, bottom = 7.dp),
                onClick = {
                    val intent = Intent(context, MainActivity::class.java)
                    intent.putExtra(DATA, mSelectedText.value)
                    startActivity(context, intent, savedInstanceState)
                })
            {
                Text(text = "Ok")
            }
            Button(modifier = Modifier
                .width(100.dp)
                .padding(7.dp),
                onClick = {
                    val intent = Intent(context, MainActivity::class.java)
                    startActivity(context, intent, savedInstanceState)
                }
            )
            {
                Text(text = "Cancel")
            }
        }
    }
}

fun getData(searchRequest: MutableState<String>,
            context: Context,
            mCities: MutableState<List<String>>,
) {
    val list = arrayListOf("")
    val url = "https://api.weatherapi.com/v1/search.json" +
            "?key=$API_KEY&" +
            "q=${searchRequest.value}"
    val queue = Volley.newRequestQueue(context)
    val stringRequest = StringRequest(
        Request.Method.GET,
        url,
        {
                response->
            val objArray = JSONArray(response)
            for (i in 0 until objArray.length()) {
                val item = objArray[i] as JSONObject
                val city = item.getString("name")
                val region = item.getString("region")
                val country = item.getString("country")
                val allData = "$city, $region, $country"

                list.add(allData)
            }
            mCities.value = list
        },
        {
            Log.d("Work","Volley error: $it")
        }
    )
    queue.add(stringRequest)
}